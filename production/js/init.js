$(document).ready(function() {
  //SVG
  svg4everybody();


  //AnimateOnScroll
  AOS.init({
    anchorPlacement: 'center-bottom',
    once: true,
    duration: 1000
  });


  //Menu
  $('.menu-trigger, .menu__close, .menu-overlay').click(function() {
  	$('.page').toggleClass('menu-on');
  });


  //Modal Video
  $(".modal").on('hidden.bs.modal', function (e) {
    $(".modal iframe").attr("src", $(".modal iframe").attr("src"));
	});


  //Sticky Header
  $(window).on("load scroll", function() {
    if ($(window).scrollTop() > 0){
      $('.page').addClass('scrolled');
    } else {
      $('.page').removeClass('scrolled');
    }
  });
});
