'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    changed = require('gulp-changed'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    rename = require('gulp-rename'),
    rimraf = require('gulp-rimraf'),
    svgmin = require('gulp-svgmin'),
    svgSymbols = require('gulp-svg-symbols'),
    fileinclude = require('gulp-file-include'),
    browserSync = require('browser-sync').create();


//SCSS
gulp.task('styles', function() {
    gulp.src('production/scss/*.scss')
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            "sourcemap=none": true
        }))
        .pipe(autoprefixer({
            browsers: ['last 4 version', 'ie 11', 'ios 6', 'android 4']
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/css/'))
        .pipe(gulp.dest('pug/dist/css/')) // Copy to pug folder
        .pipe(browserSync.stream());
});


// JS build tasks
gulp.task('js', function() {
    return gulp.src('production/js/**/*.js')
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
});


// Build markup
gulp.task('markup', function() {
    return gulp
        .src('production/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file',
            indent: 'true'
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
});


// Images
gulp.task('images', function() {
    return gulp.src('production/images/**/*.*')
        .pipe(changed('dist/images'))
        .pipe(cache(imagemin({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/images'))
        .pipe(notify({ message: 'Images task complete' }));
});


// Icons
gulp.task('icons', function() {
    return gulp.src('production/icons/*.svg')
        .pipe(svgmin({
            plugins: [{
                cleanupIDs: false
            }]
        }))
        .pipe(svgSymbols({
            templates: ['default-svg']
        }))
        .pipe(rimraf())
        .pipe(rename('icons.svg'))
        .pipe(gulp.dest('dist/icons'));
});

// Fonts
gulp.task('fonts', function() {
    return gulp.src('production/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts'))
});

// Videos
gulp.task('videos', function() {
    return gulp.src('production/videos/**/*.*')
        .pipe(gulp.dest('dist/videos'))
});


//BS
gulp.task('serve', function() {
    browserSync.init({
        server: {
            port: 3015,
            baseDir: 'dist/',
            directory: true
        }
        // startPath: "/dashboard.html"
    });

    gulp.watch('production/scss/**/*.scss', ['styles'])
        .on('change', function(event) {
            console.log('File' + event.path + ' was ' + event.type + ', running tasks...')
        });
    gulp.watch('production/**/**/*.html', ['markup']);
    gulp.watch('production/js/**/*.js', ['js']);
    gulp.watch('production/icons/**/*', ['icons']);
    gulp.watch('production/images/**/*', ['images']);
    gulp.watch('production/fonts/**/*', ['fonts']);
});

//DEFAULT
gulp.task('default', ['styles', 'js', 'markup', 'images', 'icons', 'fonts', 'videos', 'serve']);