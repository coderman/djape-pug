'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const prettify = require('gulp-html-prettify');
const plumber = require('gulp-plumber');


// HTML (markup)
const markupDist = 'dist';
gulp.task('markup', function() {
  return gulp
    .src('src/*.pug')
    .pipe(plumber())  // catch errors
    .pipe(pug({}))
    // .pipe(fileinclude({
    //   prefix: '@@',
    //   basepath: '@file',
    //   indent: 'true'
    // }))
    .pipe(prettify({
      indent_char: ' ',
      indent_size: 4}))
    .pipe(gulp.dest(markupDist))
    .pipe(browserSync.stream()); // inject into browser
});

// Browser Sync
gulp.task('browserSync', function() {
   browserSync.init({
      server: {
         baseDir: 'dist',
         directory: true,
         watchTask: true
      },
   })
})

// Watch for changes
gulp.task('watch', function(){
  gulp.watch('src/**/*.pug', ['markup']);
});

// Tasks
gulp.task('default', ['markup', 'browserSync', 'watch'], function() {
  
});